require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
   # Checking succesful run of fixture
  test "check user1" do
	assert User.where(name: 'user1').count == 1
  end
  
  # Check user creation with duplicate email
  test "check unique email" do
	user2 = User.create(email: "user1@mysite.com",password: "password")
	assert_not user2.save
  end
end
