# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
  
user1 = User.create!(name:'user1',password:'testpassword',email:'user1@mysite.com')
user2 = User.create!(name:'user2',password:'testpassword',email:'user2@mysite.com')
hotel1 = Hotel.create!(name:"HotelWalnut", description:"Eat walnuts",user:user1)
hotel2 = Hotel.create!(name:"PeachHouse", description:"Peachy peaches",user:user1)
